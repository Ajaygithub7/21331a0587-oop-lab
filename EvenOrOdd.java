import java.util.InputMismatchException;
import java.util.Scanner;

class EvenOrOdd {

    public static void evenOdd(int n) {
        System.out.print("The Given number is ");
        if (n % 2 == 0)
            System.out.println("Even");
        else
            System.out.println("Odd");
    }

    public static void main(String[] args) {
        int number;
        System.out.print("Enter a number : ");
        Scanner input = new Scanner(System.in);
        try {
            number = input.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Only integer type allowed.");
            return;
        }
        evenOdd(number);
    }
}
