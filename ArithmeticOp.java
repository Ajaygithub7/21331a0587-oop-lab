import java.util.InputMismatchException;
import java.util.Scanner;

public class ArithmeticOp {
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        int choice, a, b;
        System.out.print("1 - Addition\n2 - Substraction\n3 - Multiplication\n4 - Division\n5 - Modulo Division\n");
        System.out.print("Choose the Arithmetic operation: ");
        try {
            choice = read.nextInt();
            if (choice == 1 || choice == 2 || choice == 3 || choice == 4 || choice == 5) {
                System.out.print("Enter Operand 1(a): ");
                a = read.nextInt();
                System.out.print("Enter Operand 2(b): ");
                b = read.nextInt();
                if (choice == 1) {
                    System.out.println(a + " + " + b + " = " + (a + b));
                } else if (choice == 2) {
                    System.out.println(a + " - " + b + " = " + (a - b));
                } else if (choice == 3) {
                    System.out.println(a + " x " + b + " = " + a * b);
                } else if (choice == 4) {
                    if (b == 0) {
                        System.out.println("b can't be '0'");
                    } else
                        System.out.println(a + " / " + b + " = " + a / b);
                } else if (choice == 5) {
                    System.out.println(a + " % " + b + " = " + a % b);
                }
            } else
                System.out.println("Invalid Choice");
        } catch (InputMismatchException e) {
            System.out.println("Only integer input allowed!");
        }
    }
}
