import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.io.IOException;

public class MyByteChannel implements ByteChannel {

    private ByteBuffer buffer = ByteBuffer.allocate(1024);

    public int read(ByteBuffer dst) throws IOException {
        int bytesRead = 0;
        while (dst.remaining() > 0 && buffer.position() < buffer.limit()) {
            dst.put(buffer.get());
            bytesRead++;
        }
        System.out.println("Bytes Read : " + bytesRead);
        return bytesRead;
    }

    public int write(ByteBuffer src) throws IOException {
        int bytesWritten = 0;
        while (src.remaining() > 0 && buffer.position() < buffer.capacity()) {
            buffer.put(src.get());
            bytesWritten++;
        }
        System.out.println("Bytes written : " + bytesWritten);
        return bytesWritten;
    }

    public boolean isOpen() {
        return true;
    }

    public void close() throws IOException {
        buffer.clear();
    }

    public static void main(String[] args) throws IOException {
        MyByteChannel channel = new MyByteChannel();

        // write some data to the channel
        String data = "Hello, world!";
        ByteBuffer buf = ByteBuffer.wrap(data.getBytes());
        channel.write(buf);

        // read the data back from the channel
        ByteBuffer readBuf = ByteBuffer.allocate(1024);
        channel.read(readBuf);
        readBuf.flip();
        byte[] readData = new byte[readBuf.remaining()];
        readBuf.get(readData);
        String readString = new String(readData);
        System.out.println(readString);

        // close the channel
        channel.close();
    }
}
