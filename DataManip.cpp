// // // Write a C++ program to demonstrate the usage of data manipulators(endl, ends, ws, flush, setw, setfill, setprecision)
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;
int main()
{
    // Usage of endl
    cout << "Hello" << endl
         << "world" << endl;
    // Usage of ends
    ofstream outputFile("output.txt");
    outputFile << "This" << ends << "is" << ends << "a" << ends << "sentence.";
    outputFile.close();
    // Usage of ws
    istringstream s("     this is a test");
    string line1, line2;
    getline(s, line1);
    std::cout << "getline returns : \"" << line1 << "\"\n";
    getline(s >> ws, line2);
    std::cout << "ws + getline returns : \"" << line2 << "\"\n";
    // Usage of flush
    cout << "Flushing data " << flush << endl;
    // Usage of setw()
    string name = "CPP+java";
    double number = 12.345678;
    cout << setw(15) << name << endl;
    cout << setw(14) << name << endl;
    cout << setw(13) << name << endl;
    // Usage of setfill
    cout << setfill('*') << setw(15) << name << endl;
    // Usage of setprecision
    cout << setfill('*') << setw(15) << setprecision(4) << number << std::endl;
}
