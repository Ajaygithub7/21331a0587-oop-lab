import java.util.Scanner;

public class HelloUser {
    public static boolean string(String str) {
        int i;
        for (i = 0; i < str.length(); i++) {
            if (!((str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') || (str.charAt(i) >= 'a' && str.charAt(i) <= 'z')
                    || str.charAt(i) == ' ')) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.print("Enter your name : ");
        Scanner input = new Scanner(System.in);
        String user = input.nextLine();
        if (string(user)) {
            System.out.println("Hello " + user);
        }
    }
}