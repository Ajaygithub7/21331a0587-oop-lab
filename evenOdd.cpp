#include <iostream>
#include <cctype>
#include <string>
using namespace std;
void evenOdd(int n)
{
    cout << "The Given number is ";
    if (n % 2 == 0)
    {
        cout << "Even" << endl;
    }
    else
    {
        cout << "Odd" << endl;
    }
}
int main()
{
    string number;
    cout << "Enter any number : ";
    cin >> number;
    for (int i = 0; i < number.length(); i++)
    {
        if (isdigit(number[i]) == 0)
            return 0;
    }
    evenOdd(stoi(number));
}
