#include <iostream>
using namespace std;
int main()
{
    int choice;
    string c, d;
    int a, b;
    cout << "Choose an option : " << endl;
    cout << "1 - Addition\n2 - Substraction\n3 - Multiplication\n4 - Division\n5 - Modulo Division\n";
    cout << "Enter your option : ";
    cin >> choice;
    if (choice == 1 || choice == 2 || choice == 3 || choice == 4 || choice == 5)
    {
        cout << "Enter Operand - 1(a): ";
        cin >> c;
        cout << "Enter Operand - 2(b): ";
        cin >> d;
        for (int i = 0; i < c.length(); i++)
        {
            if (isdigit(c[i]) == 0)
            {
                cout << "Invalid Operand!" << endl;
                return 0;
            }
        }
        for (int i = 0; i < d.length(); i++)
        {
            if (isdigit(d[i]) == 0)
            {
                cout << "Invalid Operand!" << endl;
                return 0;
            }
        }
        a = stoi(c);
        b = stoi(d);
        switch (choice)
        {
        case 1:
            cout << a << " + " << b << " = " << a + b << endl;
            break;
        case 2:
            cout << a << " - " << b << " = " << a - b << endl;
            break;
        case 3:
            cout << a << " * " << b << " = " << a * b << endl;
            break;
        case 4:
            if (b == 0)
            {
                cout << "b can't be Zero(0)" << endl;
                return 0;
            }
            cout << a << " / " << b << " = " << a / b;
            break;
        case 5:
            if (b == 0)
            {
                cout << "b can't be Zero(0)" << endl;
                return 0;
            }
            cout << a << " % " << b << " = " << a % b;
            break;
        }
    }
    else
        cout << "Invalid Option" << endl;
}
